package com.example.gabor.lynxproject.activity;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.gabor.lynxproject.R;
import com.example.gabor.lynxproject.database.Contact;
import com.example.gabor.lynxproject.database.DBHelper;
import com.example.gabor.lynxproject.interfaces.GeneralCallback;
import com.example.gabor.lynxproject.util.LogService;
import com.example.gabor.lynxproject.util.Util;

/**
 * Created by gab on 21.09.2015.
 */
public class ContactDetailsActivity extends Activity implements View.OnClickListener {

    private static final String TAG = ContactDetailsActivity.class.getSimpleName();
    private static final int PICK_PHOTO_FOR_AVATAR = 222;

    private EditText firstName, lastName, phoneNumber, email, skypeID, googleID;
    private String longitude = "-100";
    private String latitude = "-100";
    private ImageView profilePicture;


    private Contact contact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact_details);

        Intent intent = this.getIntent();
        Bundle bundle = intent.getExtras();


        profilePicture = (ImageView) findViewById(R.id.contact_details_image);
        firstName = (EditText) findViewById(R.id.contact_details_first_name);
        lastName = (EditText) findViewById(R.id.contact_details_last_name);
        phoneNumber = (EditText) findViewById(R.id.contact_details_phone_number);
        email = (EditText) findViewById(R.id.contact_details_email);
        skypeID = (EditText) findViewById(R.id.contact_details_skype_id);
        googleID = (EditText) findViewById(R.id.contact_details_google_id);


        contact = new Contact();

        if (bundle.get("name") != null) {
            LogService.log(TAG, "Preview mode!");
            contact.setContactName(bundle.getString("name"));
            contact.setPhoneNumber(bundle.getString("phone"));
            contact.setLongitude(bundle.getString("long"));
            contact.setLatitude(bundle.getString("lat"));
            contact.setEmail(bundle.getString("email"));
            contact.setPicture(bundle.getByteArray("pic"));
            contact.setGoogleID(bundle.getString("gid"));
            contact.setSkypeID(bundle.getString("skypeid"));

            profilePicture.setEnabled(false);
            firstName.setKeyListener(null);
            lastName.setKeyListener(null);
            phoneNumber.setKeyListener(null);
            email.setKeyListener(null);
            skypeID.setKeyListener(null);
            googleID.setKeyListener(null);

            if (contact.getPicture() != null && contact.getPicture().length > 0) {
                LogService.log(TAG, "not null : " + contact.getPicture().length);
                profilePicture.setImageBitmap(Util.decodeBitmap(contact.getPicture()));
            } else {
                LogService.log(TAG, "null");
                profilePicture.setImageResource(R.drawable.android_image);
            }


            if (contact.getContactName() != null) {
                String[] name = contact.getContactName().split(" ");
                if (name.length > 0) {
                    firstName.setText(name[0]);
                }
                if (name.length > 1) {
                    lastName.setText(name[1]);
                }
            }
            phoneNumber.setText(contact.getPhoneNumber());
            email.setText(contact.getEmail());
            skypeID.setText(contact.getSkypeID());
            googleID.setText(contact.getGoogleID());

        }

        // Add click listeners to the buttons
        findViewById(R.id.contact_details_location_image).setOnClickListener(this);
        findViewById(R.id.contact_details_location_text).setOnClickListener(this);
        findViewById(R.id.contact_details_left_arrow).setOnClickListener(this);
        findViewById(R.id.contact_details_edit_btn).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.contact_details_location_image:
                openMap();
                break;

            case R.id.contact_details_location_text:
                openMap();
                break;

            case R.id.contact_details_left_arrow:
                finish();
                break;
            case R.id.contact_details_edit_btn:


                Intent i = new Intent(this, EditContactDetailsActivity.class);
                Bundle bundle = Util.getBundleWithContactParams(contact);
                i.putExtras(bundle);

                startActivity(i);

                finish();

                break;


        }
    }

    private Contact saveProcess() {

        contact.setContactName(firstName.getText().toString() + " " + lastName.getText().toString());
        contact.setEmail(email.getText().toString());
        contact.setPhoneNumber(phoneNumber.getText().toString());
        contact.setSkypeID(skypeID.getText().toString());
        contact.setGoogleID(googleID.getText().toString());
        contact.setLatitude(latitude);
        contact.setLongitude(longitude);
        //Note: the contact`s  setPicture is already called in OnActivityResult

        LogService.log(TAG, "latitude : " + latitude);
        LogService.log(TAG, "long : " + longitude);

        DBHelper.saveContactToDB(ContactDetailsActivity.this, contact);

        return contact;
    }

    private void openMap() {
        Intent i = new Intent(ContactDetailsActivity.this, GoogleLocationActivity.class);
        

        Bundle bundle = new Bundle();
        bundle.putString("lat",contact.getLatitude());
        bundle.putString("lng", contact.getLongitude());
        i.putExtras(bundle);

        startActivityForResult(i, 1);
    }

    public void pickImage() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, PICK_PHOTO_FOR_AVATAR);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        LogService.log(TAG, "onActivityResult : " + requestCode + " resultCode : " + resultCode + "data : " + data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                latitude = data.getStringExtra("latitude");
                longitude = data.getStringExtra("longitude");
            }

        } else if (requestCode == PICK_PHOTO_FOR_AVATAR && resultCode == Activity.RESULT_OK) {
            if (data == null) {
                return;
            }
            try {
                Uri selectedImage = data.getData();
                final String[] filePathColumn = {MediaStore.MediaColumns.DATA, MediaStore.MediaColumns.DISPLAY_NAME};
                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);

                // some devices (OS versions return an URI of com.android
                // instead of com.google.android
                if (selectedImage.toString().startsWith("content://com.android.gallery3d.provider")) {
                    // use the com.google provider, not the com.android provider.
                    selectedImage = Uri.parse(selectedImage.toString().replace("com.android.gallery3d", "com.google.android.gallery3d"));
                }

                if (cursor != null) {

                    cursor.moveToFirst();
                    int columnIndex = cursor.getColumnIndex(MediaStore.MediaColumns.DATA);
                    String filePath = cursor.getString(columnIndex);
                    cursor.close();

                    //Show a progress dialog
                   /* final ProgressDialog progress=new ProgressDialog(this);
                    progress.setMessage("Hold on");
                    progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                    progress.setIndeterminate(true);
                    progress.setProgress(0);
                    progress.show();*/

                    //LogService.log(TAG,"profilePicture.getWidth() : " + profilePicture.getWidth() + " " + profilePicture.getHeight());

                    // Resize the Bitmap to the imageView`s size
                    Util.getResizedBitmap(profilePicture.getWidth(), profilePicture.getHeight(), filePath, new GeneralCallback() {
                        @Override
                        public void Callback(Object o) {
                            Bitmap bitmap = (Bitmap) o;
                            profilePicture.setImageBitmap(bitmap);
                            contact.setPicture(Util.getBitmapAsByteArray(bitmap));
                            // progress.hide();
                        }
                    });

                }
            } catch (Exception e) {
                Toast.makeText(ContactDetailsActivity.this, "Profile picture not found", Toast.LENGTH_SHORT).show();
            }

        } else if (requestCode == ContactListActivity.NEW_CONTACT_WAS_ADDED && resultCode == ContactListActivity.NEW_CONTACT_WAS_ADDED) {
            Contact  newContact = (Contact)data.getExtras().getSerializable("Contact");
            Intent returnIntent = getIntent();
            returnIntent.putExtra("Contact", newContact);
            setResult(ContactListActivity.NEW_CONTACT_WAS_ADDED, returnIntent);

            finish();
        }


    }//onActivityResult


}
