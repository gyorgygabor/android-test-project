package com.example.gabor.lynxproject.database;

import android.content.Context;

import com.example.gabor.lynxproject.util.LogService;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by gab on 21.09.2015.
 */
public class DBHelper {


    private static final String TAG = DBHelper.class.getSimpleName();

    public static void saveContactsToDB(Context con, ArrayList<Contact> _contacts) {
        // Open the default realm for the UI thread.
        if (con != null) {

            Realm realm = Realm.getInstance(con);

            realm.beginTransaction();
            for (Contact contact : _contacts) {
                Contact realmContactObject = realm.createObject(Contact.class);
                realmContactObject.setContactName(contact.getContactName());
                realmContactObject.setPhoneNumber(contact.getPhoneNumber());
            }
            realm.commitTransaction();
        }
    }

    public static void saveContactToDB(Context con, Contact phoneContact) {
        LogService.log(TAG, "saveContactToDB : " + phoneContact);

        // Open the default realm for the UI thread.
        if (con != null) {

            Realm realm = Realm.getInstance(con);

            realm.beginTransaction();
            Contact c = realm.createObject(phoneContact.getClass());
            LogService.log(TAG, "save to db : " + phoneContact.getContactName());
            LogService.log(TAG, "save to db : " + phoneContact.getEmail());

            c.setContactName(phoneContact.getContactName() == null ? "" : phoneContact.getContactName());
            c.setPhoneNumber(phoneContact.getPhoneNumber() == null ? "" : phoneContact.getPhoneNumber());

            c.setEmail(phoneContact.getEmail() == null ? "" : phoneContact.getEmail());
            c.setSkypeID(phoneContact.getSkypeID() == null ? "" : phoneContact.getSkypeID());
            c.setGoogleID(phoneContact.getGoogleID() == null ? "" : phoneContact.getGoogleID());
            c.setLatitude(phoneContact.getLatitude() == null ? "" : phoneContact.getLatitude());
            c.setLongitude(phoneContact.getLongitude() == null ? "" : phoneContact.getLongitude());
            realm.commitTransaction();
        }
    }

    public static ArrayList<Contact> getAllContacts(Context con) {
        if (con != null) {
            Realm realm = Realm.getInstance(con);
            RealmResults<Contact> contacts = realm.allObjects(Contact.class);
            ArrayList<Contact> contactsArrayList = new ArrayList<Contact>();
            contactsArrayList.addAll(contacts.subList(0, contacts.size()));
            return contactsArrayList;
        }
        return null;
    }


    public static Boolean isDBSet(Context con) {
        int objectsSize = 0;
        if (con != null) {
            Realm realm = Realm.getInstance(con);
            objectsSize = realm.allObjects(Contact.class).size();
        }
        return objectsSize <= 0 ? false : true;
    }


    public static void closeDB(Context con) {
        if (con != null) {
            Realm.getInstance(con).close();
        }
    }

    public static void updateContactInDB(Context con, Contact phoneContact) {
        if (con != null && false) {
            Realm realm = Realm.getInstance(con);
            Contact c = realm.where(Contact.class).equalTo("phoneNumber", phoneContact.getPhoneNumber()).findFirst();


            // Update person in a write transaction
            realm.beginTransaction();

            c.setContactName(phoneContact.getContactName() == null ? "" : phoneContact.getContactName());
            //c.setPhoneNumber(phoneContact.getPhoneNumber() == null ? "" : phoneContact.getPhoneNumber());

            c.setEmail(phoneContact.getEmail() == null ? "" : phoneContact.getEmail());
            c.setSkypeID(phoneContact.getSkypeID() == null ? "" : phoneContact.getSkypeID());
            c.setGoogleID(phoneContact.getGoogleID() == null ? "" : phoneContact.getGoogleID());
            c.setLatitude(phoneContact.getLatitude() == null ? "" : phoneContact.getLatitude());
            c.setLongitude(phoneContact.getLongitude() == null ? "" : phoneContact.getLongitude());
            //contactResult.setAge(99);

            realm.commitTransaction();
        }
    }
}
