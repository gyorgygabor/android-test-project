package com.example.gabor.lynxproject.database;

import java.io.Serializable;

import io.realm.RealmObject;

/**
 * Created by Gabor on 19.09.2015.
 */
public class Contact extends RealmObject implements Serializable {

    private String contactName;
    private String phoneNumber;
    private String email;
    private String skypeID;
    private String googleID;
    private String latitude;
    private String longitude;


    public byte[] getPicture() {
        return picture;
    }

    public void setPicture(byte[] picture) {
        this.picture = picture;
    }

    private byte[] picture;


    public Contact() {

    }

    public Contact(String _contactName, String _phoneNumber) {

        contactName = _contactName;
        phoneNumber = _phoneNumber;
    }

    public String getContactName() {
        return contactName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }


    public String getSkypeID() {
        return skypeID;
    }

    public void setSkypeID(String skypeID) {
        this.skypeID = skypeID;
    }

    public String getGoogleID() {
        return googleID;
    }

    public void setGoogleID(String googleID) {
        this.googleID = googleID;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}

