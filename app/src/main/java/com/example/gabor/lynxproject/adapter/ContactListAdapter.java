package com.example.gabor.lynxproject.adapter;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.gabor.lynxproject.R;
import com.example.gabor.lynxproject.activity.ContactDetailsActivity;
import com.example.gabor.lynxproject.activity.ContactListActivity;
import com.example.gabor.lynxproject.database.Contact;
import com.example.gabor.lynxproject.interfaces.CustomOnClickListener;
import com.example.gabor.lynxproject.util.LogService;
import com.example.gabor.lynxproject.util.Util;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Gabor on 18.09.2015.
 */
public class ContactListAdapter extends BaseAdapter {
    private static final String TAG = ContactListAdapter.class.getSimpleName();
    private ArrayList<Contact> contacts;
    private LayoutInflater inflater;
    private CustomOnClickListener.OnCustomClickListener tapCallback;


    public ContactListAdapter(final ContactListActivity con, final ArrayList<Contact> contactList) {
        inflater = LayoutInflater.from(con);
        contacts = contactList;

        for (int i = 0; i < contacts.size(); ++i) {
            if (i % 3 == 0) {
                contacts.add(i, new Contact());
            }
        }

        tapCallback = new CustomOnClickListener.OnCustomClickListener() {
            @Override
            public void OnCustomClick(View aView, int id) {

                if (aView.getId() == R.id.contact_list_item_text_container) {
                    Intent i = new Intent(con, ContactDetailsActivity.class);

                    LogService.log(TAG, "contact : " + (Contact) getItem(id));
                    Contact contact = (Contact) getItem(id);
                    Bundle bundle = Util.getBundleWithContactParams(contact);

                    i.putExtras(bundle);

                    con.startActivity(i);
                } else if (aView.getId() == R.id.contact_item_refresh_image) {
                    Contact contact = (Contact) getItem(id);

                    Util.updateContact(con, contact);
                    //refreshListAdapter();
                }

            }
        };
    }


    /**
     * How many items are in the data set represented by this Adapter.
     *
     * @return Count of items.
     */
    @Override
    public int getCount() {
        if (contacts != null) {
            return contacts.size();
        } else {
            return 0;
        }

    }

    /**
     * Get the data item associated with the specified position in the data set.
     *
     * @param position Position of the item whose data we want within the adapter's
     *                 data set.
     * @return The data at the specified position.
     */
    @Override
    public Object getItem(int position) {

        if (contacts != null) {
            return contacts.get(position);
        }
        return null;
    }

    /**
     * Get the row id associated with the specified position in the list.
     *
     * @param position The position of the item within the adapter's data set whose row id we want.
     * @return The id of the item at the specified position.
     */
    @Override
    public long getItemId(int position) {
        return 0;
    }

    /**
     * Get a View that displays the data at the specified position in the data set. You can either
     * create a View manually or inflate it from an XML layout file. When the View is inflated, the
     * parent View (GridView, ListView...) will apply default layout parameters unless you use
     * {@link LayoutInflater#inflate(int, ViewGroup, boolean)}
     * to specify a root view and to prevent attachment to the root.
     *
     * @param position    The position of the item within the adapter's data set of the item whose view
     *                    we want.
     * @param convertView The old view to reuse, if possible. Note: You should check that this view
     *                    is non-null and of an appropriate type before using. If it is not possible to convert
     *                    this view to display the correct data, this method can create a new view.
     *                    Heterogeneous lists can specify their number of view types, so that this View is
     *                    always of the right type (see {@link #getViewTypeCount()} and
     *                    {@link #getItemViewType(int)}).
     * @param parent      The parent that this view will eventually be attached to
     * @return A View corresponding to the data at the specified position.
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ContactListViewHolder viewHolder;

        Contact contact = (Contact) getItem(position);
        //final String phoneNumber = contact.getPhoneNumber();
        //LogService.log("ADAPTER",contact.toString());

        // viewHolder init
        if (convertView == null) {


            if(contact.getContactName()== null && contact.getPhoneNumber() == null ){
                convertView = inflater.inflate(R.layout.red_item, null);
                convertView.setTag("red");

            }else {
                convertView = inflater.inflate(R.layout.contact_list_item, null);
                viewHolder = new ContactListViewHolder();

                viewHolder.contactName = (TextView) convertView.findViewById(R.id.contact_list_item_textfield);
                viewHolder.avatarImage = (ImageView) convertView.findViewById(R.id.contact_item_image);
                viewHolder.refreshImage = (ImageView) convertView.findViewById(R.id.contact_item_refresh_image);

                convertView.setTag(viewHolder);
            }
        } else {

            if (convertView.getTag() instanceof String) {
               // convertView = inflater.inflate(R.layout.red_item, null);
                //convertView.setTag("red");
            } else {
                viewHolder = (ContactListViewHolder) convertView.getTag();

                //Add click listeners to the items
                convertView.findViewById(R.id.contact_item_refresh_image).setOnClickListener(new CustomOnClickListener(tapCallback, position));
                convertView.findViewById(R.id.contact_list_item_text_container).setOnClickListener(new CustomOnClickListener(tapCallback, position));


                String name = contact.getContactName();
                byte[] avatarImageByteArray = contact.getPicture();

                // Populate the item
                viewHolder.contactName.setText(name);
                viewHolder.avatarImage.setTag(avatarImageByteArray);
                if (avatarImageByteArray != null && avatarImageByteArray.length > 0) {
                    LogService.log(TAG, "avatarImageByteArray : " + avatarImageByteArray);
                    viewHolder.avatarImage.setImageBitmap(Util.decodeBitmap(avatarImageByteArray));
                } else {
                    viewHolder.avatarImage.setImageResource(R.drawable.android_image);
                }

            }


        }


        return convertView;
    }

    public boolean containsContact(Contact contactToCompare) {

        for (Contact contact : contacts) {
            if (Util.compareContact(contact, contactToCompare)) {
                return true;
            }
        }
        return false;
    }

    public void clear() {
        if (contacts != null) {
            contacts.clear();
        }
    }

    public void setContacts(ArrayList<Contact> list) {
        contacts = list;
    }

    public void addContactToAdapter(Contact contact) {
        contacts.add(contact);
    }

    public void refreshListAdapter() {
        Collections.sort(contacts, Util.getContactComparator());
        notifyDataSetChanged();
    }

    // Update the contact
    public void updateContact(Contact contact) {
        LogService.log(TAG, "in updateContact");

        for (Contact _contact : contacts) {
            if (contact.getPhoneNumber().compareTo(_contact.getPhoneNumber()) == 0) {
                LogService.log(TAG, "in updateContact if");
                _contact = contact;
                break;
            }
        }
    }


    static class ContactListViewHolder {

        public TextView contactName;

        public ImageView refreshImage, avatarImage;
    }
}
