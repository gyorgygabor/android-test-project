package com.example.gabor.lynxproject.util;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

public class LogService {
	public static final int LOG_LEVEL_FULL = 0;

	public static final int LOG_LEVEL_LITE = 1;

	private static final boolean LOG_ENABLED = true;

	private static final boolean ERR_LOG_ENABLED = false;

	public static void log(String tag, String msg) {
		if (LOG_ENABLED) {
			Log.i(tag, "---:" + msg);
		}
	}

	public static void err(String tag, String msg, Throwable e, int LOG_LEVEL) {
		try {
			if (ERR_LOG_ENABLED) {
				Log.e(tag, msg, e);
			}
		} catch (Exception e1) {// DO NOTHING
		}
	}

	public static void toast(final Context ctx, final String msg) {
		if (ctx != null) {
			((Activity) ctx).runOnUiThread(new Runnable() {
				@Override
				public void run() {
					Toast.makeText(ctx, msg, Toast.LENGTH_SHORT).show();
				}
			});
		}
	}

	public static void memoryCheck(String tag) {
		if (LOG_ENABLED) {
			Log.i(tag, "==============================");
			Log.i(tag, ">> Free memory: " + (Runtime.getRuntime().freeMemory() / 1024) + " Kb");
			Log.i(tag, ">> Max memory: " + (Runtime.getRuntime().maxMemory() / 1024) + " Kb");
			Log.i(tag, ">> Total memory: " + (Runtime.getRuntime().totalMemory() / 1024) + " Kb");
			Log.i(tag, "==============================");
		}
	}
}