package com.example.gabor.lynxproject.entities;

import com.example.gabor.lynxproject.database.Contact;

/**
 * Created by gab on 23.09.2015.
 */
public class ContactResponse {
    public Contact contact;
    public boolean TAG;

    public ContactResponse(Contact _contact, boolean _TAG){
        contact = _contact;
        TAG = _TAG;

    }
}
