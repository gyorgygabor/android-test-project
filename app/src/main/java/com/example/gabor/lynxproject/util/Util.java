package com.example.gabor.lynxproject.util;

import android.accounts.AccountManager;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.Context;
import android.content.OperationApplicationException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.util.Patterns;
import android.widget.Toast;

import com.example.gabor.lynxproject.database.Contact;
import com.example.gabor.lynxproject.interfaces.GeneralCallback;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Locale;
import java.util.regex.Pattern;

/**
 * Created by Gabor on 18.09.2015.
 */
public class Util {

    public static final String TAG = Util.class.getSimpleName();



 /*   public static void getFriends(final Context con, final FriendsListener l) {

        new ExecutorAsyncTask<Void, Void, Void>() {
            private Friends frend1;

            private ArrayList<Friends> friendslist;

            @Override
            protected Void doInBackground(Void... params) {

                friendslist = new ArrayList<Friends>();



                    //friendslist.add(frend1);



                ContentResolver cr = con.getContentResolver();
                Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                        null, null, null, null);
                if (cur.getCount() > 0) {
                    while (cur.moveToNext()) {
                        String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                        String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                        if (Integer.parseInt(cur.getString(
                                cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                            Cursor pCur = cr.query(
                                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                    null,
                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = ?",
                                    new String[]{id}, null);
                            while (pCur.moveToNext()) {
                                String phoneNo = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                                Toast.makeText(con, "Name: " + name + ", Phone No: " + phoneNo, Toast.LENGTH_SHORT).show();
                            }
                            pCur.close();
                        }
                    }
                }


                Collections.sort(friendslist, new Comparator<Friends>() {
                    @Override
                    public int compare(Friends user1, Friends user2) {

                        return user1.getContactName().toUpperCase(Locale.getDefault()).compareTo(user2.getContactName().toUpperCase(Locale.getDefault()));
                    }
                });

                LogService.log(TAG, "**Number Users: " + roster.getEntryCount());
                return null;
            }

            protected void onPostExecute(Void result) {

                l.onFinish(friendslist);

            };
        }.execute();
    }
*/

    /**
     * Returns the used email adress
     * *
     */
    public static String getUserEmail(Context con) {


        AccountManager manager = AccountManager.get(con);
        android.accounts.Account[] accounts = manager.getAccountsByType("com.google");
        // Account[] accounts = AccountManager.get(con).getAccounts();


        Pattern emailPattern = Patterns.EMAIL_ADDRESS;

        for (android.accounts.Account account : accounts) {
            if (emailPattern.matcher(account.name).matches()) {

                return account.name;
            }
        }

        return null;

    }

    public static Comparator<Contact> getContactComparator() {

        return new Comparator<Contact>() {
            @Override
            public int compare(Contact user1, Contact user2) {

                return user1.getContactName().toUpperCase(Locale.getDefault()).compareTo(user2.getContactName().toUpperCase(Locale.getDefault()));
            }
        };
    }

    public static Boolean compareContact(Contact c1, Contact c2) {

        return (c1.getPhoneNumber().compareTo(c2.getPhoneNumber()) == 0 && c1.getContactName().compareTo(c2.getContactName()) == 0) ? true : false;
    }

    public static byte[] getBitmapAsByteArray(Bitmap photo) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        photo.compress(Bitmap.CompressFormat.PNG, 0, bos);
        return bos.toByteArray();
    }

    public static Bitmap decodeBitmap(byte[] imgByte) {
        return BitmapFactory.decodeByteArray(imgByte, 0, imgByte.length);
    }

    public static void getResizedBitmap(final int targetW, final int targetH, final String imagePath, final GeneralCallback callbak) {

        new AsyncTask<Void, Bitmap, Bitmap>() {


            @Override
            protected Bitmap doInBackground(Void... params) {
                // Get the dimensions of the bitmap
                BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                //inJustDecodeBounds = true <-- will not load the bitmap into memory
                bmOptions.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(imagePath, bmOptions);
                int photoW = bmOptions.outWidth;
                int photoH = bmOptions.outHeight;

                // Determine how much to scale down the image
                int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

                // Decode the image file into a Bitmap sized to fill the View
                bmOptions.inJustDecodeBounds = false;
                bmOptions.inSampleSize = scaleFactor;
                bmOptions.inPurgeable = true;

                Bitmap bitmap = BitmapFactory.decodeFile(imagePath, bmOptions);
                return bitmap;
            }

            @Override
            protected void onPostExecute(Bitmap aVoid) {
                super.onPostExecute(aVoid);
                if (callbak != null) {
                    callbak.Callback(aVoid);
                }


            }
        }.execute();


    }


    public static void saveContactToPhone(Context context, Contact cont) {
        ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();
        int rawContactInsertIndex = ops.size();

        ops.add(ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI)
                .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)
                .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null).build());

        //Phone Number
        ops.add(ContentProviderOperation
                .newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID,
                        rawContactInsertIndex)
                .withValue(ContactsContract.Contacts.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, cont.getPhoneNumber())
                .withValue(ContactsContract.Contacts.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.Phone.TYPE, "1").build());

        //Display name/Contact name
        ops.add(ContentProviderOperation
                .newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Contacts.Data.RAW_CONTACT_ID,
                        rawContactInsertIndex)
                .withValue(ContactsContract.Contacts.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, cont.getContactName())
                .build());
        //Email details
        ops.add(ContentProviderOperation
                .newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID,
                        rawContactInsertIndex)
                .withValue(ContactsContract.Contacts.Data.MIMETYPE, ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.Email.DATA, cont.getEmail())
                .withValue(ContactsContract.Contacts.Data.MIMETYPE, ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.Email.TYPE, "2").build());


        //Postal Address
/*
        ops.add(ContentProviderOperation
                .newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID,
                        rawContactInsertIndex)
                .withValue(ContactsContract.Contacts.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.StructuredPostal.POBOX, "Postbox")

                .withValue(ContactsContract.Contacts.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.StructuredPostal.STREET, "street")

                .withValue(ContactsContract.Contacts.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.StructuredPostal.CITY, "city")

                .withValue(ContactsContract.Contacts.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.StructuredPostal.REGION, "region")

                .withValue(ContactsContract.Contacts.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.StructuredPostal.POSTCODE, "postcode")

                .withValue(ContactsContract.Contacts.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.StructuredPostal.COUNTRY, "country")

                .withValue(ContactsContract.Contacts.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.StructuredPostal.TYPE, "3")


                .build()); */


        //Organization details
      /*  ops.add(ContentProviderOperation
                .newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Contacts.Data.RAW_CONTACT_ID,
                        rawContactInsertIndex)
                .withValue(ContactsContract.Contacts.Data.MIMETYPE, ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.Organization.COMPANY, "Devindia")
                .withValue(ContactsContract.Contacts.Data.MIMETYPE, ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.Organization.TITLE, "Developer")
                .withValue(ContactsContract.Contacts.Data.MIMETYPE, ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.Organization.TYPE, "0")

                .build());*/
        //IM details
        ops.add(ContentProviderOperation
                .newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Contacts.Data.RAW_CONTACT_ID,
                        rawContactInsertIndex)
                .withValue(ContactsContract.Contacts.Data.MIMETYPE, ContactsContract.CommonDataKinds.Im.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.Im.DATA, "Skype ID : " + cont.getSkypeID())
                .withValue(ContactsContract.Contacts.Data.MIMETYPE, ContactsContract.CommonDataKinds.Im.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.Im.DATA5, "2")


                .build());
        try {
            ContentProviderResult[] res = context.getContentResolver().applyBatch(
                    ContactsContract.AUTHORITY, ops);
        } catch (Exception e) {
            Toast.makeText(context,"Contact can not save to Phone",Toast.LENGTH_SHORT).show();
        }


    }

    public static Bundle getBundleWithContactParams(Contact contact) {

        Bundle bundle = new Bundle();
        bundle.putString("name", contact.getContactName());
        bundle.putString("email", contact.getEmail());
        bundle.putString("gid", contact.getGoogleID());
        bundle.putString("lat", contact.getLatitude());
        bundle.putString("long", contact.getLongitude());
        bundle.putString("phone", contact.getPhoneNumber());
        bundle.putString("skypeid", contact.getSkypeID());
        bundle.putByteArray("pic", contact.getPicture());

        return bundle;

    }


    public static void updateContact(Context context, Contact contact){
        try {
            ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();

            ops.add(ContentProviderOperation.newUpdate(ContactsContract.Data.CONTENT_URI)
                    .withSelection(ContactsContract.CommonDataKinds.Phone.NUMBER + "=? AND " +
                                    ContactsContract.Contacts.Data.MIMETYPE + "='" +
                                    ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE + "'",
                            new String[]{contact.getPhoneNumber()})
                    .withValue(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, contact.getContactName())

                    .build());

            //.withSelection(ContactsContract.CommonDataKinds.Phone.NUMBER + " = ?", new String[]{contact.getPhoneNumber()})
            //withValue(ContactsContract.Contacts.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
            ContentProviderResult[] result = context.getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
        } catch (Exception e) {

            Toast.makeText(context, "Update failed", Toast.LENGTH_SHORT).show();
        }
    }
}
