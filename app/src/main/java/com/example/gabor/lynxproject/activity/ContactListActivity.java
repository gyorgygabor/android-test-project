package com.example.gabor.lynxproject.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.AttributeSet;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.example.gabor.lynxproject.R;
import com.example.gabor.lynxproject.adapter.ContactListAdapter;
import com.example.gabor.lynxproject.async.ContactListTask;
import com.example.gabor.lynxproject.database.Contact;
import com.example.gabor.lynxproject.database.DBHelper;
import com.example.gabor.lynxproject.entities.ContactResponse;
import com.example.gabor.lynxproject.interfaces.ContactsListener;
import com.example.gabor.lynxproject.util.LogService;
import com.example.gabor.lynxproject.util.Util;

import java.util.ArrayList;
import java.util.Collections;

import de.greenrobot.event.EventBus;

public class ContactListActivity extends Activity implements SwipeRefreshLayout.OnRefreshListener, View.OnClickListener {

    private static final String TAG = ContactListActivity.class.getSimpleName();
    public static final int NEW_CONTACT_WAS_ADDED = 123;

    private ListView contactListView;
    private ContactListAdapter listAdapter;

    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact_list);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh);
        findViewById(R.id.contact_list_plus_btn).setOnClickListener(this);
        swipeRefreshLayout.setOnRefreshListener(this);

        contactListView = (ListView) findViewById(R.id.contacts_listview);


        // If the DB is set, get the contacts from DB
        if (DBHelper.isDBSet(ContactListActivity.this)) {

            LogService.log(TAG, "isDBSet true");
            Toast.makeText(this, "isDBSet true", Toast.LENGTH_SHORT).show();

            // Get the contacts from DB
            ArrayList<Contact> contactsArrayList = DBHelper.getAllContacts(ContactListActivity.this);

            Collections.sort(contactsArrayList, Util.getContactComparator());

            // Populate the list
            populateTheList(contactsArrayList);

        } else {

            LogService.log(TAG, "isDBSet false");
            Toast.makeText(this, "isDBSet false", Toast.LENGTH_SHORT).show();
            // Get the contacts from the phone
            new ContactListTask(this, new ContactsListener() {
                @Override
                public void onFinish(ArrayList<Contact> contacts) {
                    if (contacts != null) {
                        // Save the contacts to DB
                        DBHelper.saveContactsToDB(ContactListActivity.this, contacts);
                        // Populate the list
                        populateTheList(contacts);
                    }
                }
            }).execute();
        }
    }


    @Override
    public View onCreateView(View parent, String name, Context context, AttributeSet attrs) {
        return super.onCreateView(parent, name, context, attrs);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }


    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void populateTheList(ArrayList<Contact> elements) {

        listAdapter = new ContactListAdapter(this, elements);

        contactListView.setAdapter(listAdapter);
        listAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        // Close the DB when done.
        DBHelper.closeDB(ContactListActivity.this);
    }

    @Override
    public void onRefresh() {
        syncListWithSim();
    }

    private void syncListWithSim() {

        // Get the contacts from the phone
        new ContactListTask(this, new ContactsListener() {
            @Override
            public void onFinish(ArrayList<Contact> contacts) {

                //realm.clear(Contact.class);


                Boolean listChanged = false;

                for (Contact phoneContact : contacts) {


                    if (!listAdapter.containsContact(phoneContact)) {


                        // Add the new contact to list adapter
                        listAdapter.addContactToAdapter(phoneContact);

                        // Add the new contact to DB
                        DBHelper.saveContactToDB(ContactListActivity.this, phoneContact);

                        LogService.log(TAG, "new contact was added : " + phoneContact.getContactName() + " " + phoneContact.getPhoneNumber());

                        listChanged = true;
                    }
                }

                // If new contact was added, refresh the list
                if (listChanged) {
                    listAdapter.refreshListAdapter();
                }

                // Stop the pull to refresh loading
                swipeRefreshLayout.setRefreshing(false);
            }
        }).execute();


    }


    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.contact_list_plus_btn) {

            Intent i = new Intent(ContactListActivity.this, EditContactDetailsActivity.class);

            startActivity(i);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        LogService.log(TAG, "onActivityResult : " + requestCode + " resultCode : " + resultCode + "data : " + data);
        if (requestCode == NEW_CONTACT_WAS_ADDED) {
            if (resultCode == NEW_CONTACT_WAS_ADDED) {
                Contact newContact = (Contact) data.getExtras().getSerializable("Contact");
                addNewContactToList(newContact);
            }

        }
    }


    @Override
    public void onStop() {

        super.onStop();
        LogService.log(TAG, "onStop");

    }

    public void onEvent(Contact contact) {
        LogService.log(TAG, "on event : " + contact.getContactName());
        // Get the contacts from DB
        ArrayList<Contact> contactsArrayList = DBHelper.getAllContacts(ContactListActivity.this);

        Collections.sort(contactsArrayList, Util.getContactComparator());

        // Populate the list
        populateTheList(contactsArrayList);
        Toast.makeText(this, "New contact is added", Toast.LENGTH_SHORT).show();
    }

    //Called when a contact is updated
    public void onEvent(ContactResponse contactResponse) {
        LogService.log(TAG, "on event : " + contactResponse.TAG);

        if (contactResponse.TAG) {
            // Get the contacts from DB
            listAdapter.updateContact(contactResponse.contact);
            listAdapter.refreshListAdapter();

            Toast.makeText(this, "The contact is updated", Toast.LENGTH_SHORT).show();
        } else {
            addNewContactToList(contactResponse.contact);
        }


    }

    public void addNewContactToList(Contact newContact) {

        LogService.log(TAG, "newContact : " + newContact.getContactName());
        listAdapter.addContactToAdapter(newContact);
        listAdapter.refreshListAdapter();

    }

    public void refreshListAdapter() {
        listAdapter.refreshListAdapter();
    }
}
