package com.example.gabor.lynxproject.interfaces;

import com.example.gabor.lynxproject.database.Contact;

import java.util.ArrayList;

/**
 * Created by Gabor on 19.09.2015.
 */
public interface ContactsListener {
    void onFinish(ArrayList<Contact> contacts);
}
