package com.example.gabor.lynxproject.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.gabor.lynxproject.R;
import com.example.gabor.lynxproject.util.LogService;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;

/**
 * Created by gab on 20.09.2015.
 */
public class GoogleLocationActivity extends Activity implements GoogleMap.OnMapClickListener, View.OnClickListener, android.location.LocationListener {

    private static String TAG = GoogleLocationActivity.class.getSimpleName();
    private GoogleMap map;

    private static final long MIN_TIME = 400;
    private static final float MIN_DISTANCE = 1000;
    private LocationManager locationManager;
    private LatLng position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map);
        findViewById(R.id.map_done_button).setOnClickListener(this);



        try {

            Intent intent = this.getIntent();
            Bundle bundle = intent.getExtras();

            map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME, MIN_DISTANCE, this); //You can also use LocationManager.GPS_PROVIDER and LocationManager.PASSIVE_PROVIDER


            LogService.log(TAG,"bundle : " + bundle);
            if(bundle != null){
                String lat = bundle.getString("lat");
                String lng = bundle.getString("lng");

                LogService.log(TAG, "lat:" +lat);
                LogService.log(TAG, "lng:" + lng);

                if(lat != null && lng != null){
                    map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(getLocation(lat), getLocation(lng)), 15));
                }
            }else{
                map.setOnMapClickListener(this);
                map.setMyLocationEnabled(true);
            }

        } catch (Exception e) {
            Toast.makeText(GoogleLocationActivity.this, "Map not found", Toast.LENGTH_SHORT);
        }

        checkLocationServiceStatus();
    }


    @Override
    public void onMapClick(LatLng latLng) {
        
        position = latLng;
        String address = "";
        map.clear();

        try {
            // get the clicked location address
            List<Address> locationAddress = new Geocoder(this).getFromLocation(latLng.latitude, latLng.longitude, 3);

            for (Address addressElement : locationAddress) {
                for (int i = 0; i <= addressElement.getMaxAddressLineIndex(); ++i) {
                    address += " " + addressElement.getAddressLine(i);
                }

                if (address.length() > 0) {
                    break;
                }
            }

            // add marker to the map
            map.addMarker(new MarkerOptions().position(latLng).title(address == null ? "Address not found" : address).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));

            LogService.log(TAG, locationAddress.toString());
            LogService.log(TAG, latLng.toString());

        } catch (Exception e) {
            Toast.makeText(GoogleLocationActivity.this, "Location not found. Please enable your GPS and Network Connection", Toast.LENGTH_LONG);
        }
    }


    @Override
    public void onClick(View view) {

        // Done button click
        if (view.getId() == R.id.map_done_button) {

            Intent returnIntent = new Intent();

            if(position != null) {
                returnIntent.putExtra("latitude", ""+position.latitude);
                returnIntent.putExtra("longitude", ""+position.longitude);
                setResult(RESULT_OK, returnIntent);
            }else{
                setResult(RESULT_CANCELED, returnIntent);
            }

            finish();

        }
    }

    @Override
    public void onLocationChanged(Location location) {

        Bundle bundle = getIntent().getExtras();
        if(bundle != null) {
            LogService.log(TAG, "onLocationChanged " + location.toString());
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 15));
            locationManager.removeUpdates(this);
        }
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }


    private double getLocation(String loc){
        double value = Double.parseDouble( loc.replace(",",".") );
        return  value;
    }
    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }


    // Check if the Location Service is enabled
    private void checkLocationServiceStatus() {
        LocationManager lm = (LocationManager) GoogleLocationActivity.this.getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
            Toast.makeText(GoogleLocationActivity.this, "Location service is not available.", Toast.LENGTH_SHORT);
        }


        if (!gps_enabled && !network_enabled) {
            // notify user
            AlertDialog.Builder dialog = new AlertDialog.Builder(GoogleLocationActivity.this);
            dialog.setMessage("GPS location is not enabled");
            dialog.setPositiveButton("Open location settings", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    GoogleLocationActivity.this.startActivity(myIntent);

                }
            });
            dialog.setNegativeButton("Cancel", null);
            dialog.show();
        }
    }

}
