package com.example.gabor.lynxproject.async;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.provider.ContactsContract;

import com.example.gabor.lynxproject.database.Contact;
import com.example.gabor.lynxproject.interfaces.ContactsListener;
import com.example.gabor.lynxproject.util.LogService;
import com.example.gabor.lynxproject.util.Util;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Gabor on 19.09.2015.
 */
public class ContactListTask extends AsyncTask<String, Void, ArrayList<Contact>> {

    private static final String TAG = RetrieveTokenTask.class.getSimpleName();

    ContentResolver cr;
    private Context context;
    private String accountEmail;
    ContactsListener listener;
    private ArrayList<Contact> contactlist;


    public ContactListTask(Context con, ContactsListener l) {
        context = con;
        listener = l;
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        accountEmail = Util.getUserEmail(context);
        cr = context.getContentResolver();
    }

    // make the default Constructor to private
    private ContactListTask() {
    }


    @Override
    protected ArrayList<Contact> doInBackground(String... params) {


        contactlist = new ArrayList<Contact>();


        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);

        if (cur.getCount() > 0) {
            while (cur.moveToNext()) {
                String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                if (Integer.parseInt(cur.getString(
                        cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                    Cursor pCur = cr.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{id}, null);
                    while (pCur.moveToNext()) {
                        String phoneNo = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        String contactName = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME_PRIMARY));

                        //String skype = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.));

                        contactlist.add(new Contact(contactName, phoneNo));

                        LogService.log(TAG, "phoneNo : " + phoneNo);
                        // Toast.makeText(con, "Name: " + name + ", Phone No: " + phoneNo, Toast.LENGTH_SHORT).show();
                    }
                    pCur.close();
                }
            }
        }

        Collections.sort(contactlist, Util.getContactComparator());

        return contactlist;
    }

    @Override
    protected void onPostExecute(ArrayList<Contact> result) {
        super.onPostExecute(result);

        if (listener != null) {
            listener.onFinish(result);
        }
    }
}
