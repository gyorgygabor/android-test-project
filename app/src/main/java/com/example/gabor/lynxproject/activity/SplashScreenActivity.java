package com.example.gabor.lynxproject.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.example.gabor.lynxproject.R;
import com.example.gabor.lynxproject.async.RetrieveTokenTask;
import com.example.gabor.lynxproject.interfaces.GeneralCallback;
import com.example.gabor.lynxproject.util.Constants;


public class SplashScreenActivity extends Activity implements GeneralCallback {

    private static final String TAG = SplashScreenActivity.class.getSimpleName();

    public static final int REQ_SIGN_IN_REQUIRED = 55664;

    private String mAccountName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);



        new RetrieveTokenTask(this,this).execute();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQ_SIGN_IN_REQUIRED && resultCode == RESULT_OK) {
            // We had to sign in - now we can finish off the token request.
            new RetrieveTokenTask(this,this).execute();
        }
    }


    //Get Google UID Callback
    @Override
    public void Callback(Object o) {

        Toast.makeText(this,"Your Google ID is:" + Constants.GOOGLE_UID,Toast.LENGTH_LONG).show();
        navigateToContactListActivity(4000);
    }

   private void navigateToContactListActivity(int delay){
       findViewById(R.id.splashscreen_logo).postDelayed(new Runnable() {
           @Override
           public void run() {

               Intent i = new Intent(SplashScreenActivity.this,ContactListActivity.class);
               startActivity(i);
               finish();
           }
       }, delay);
   }

}
