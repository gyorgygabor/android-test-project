package com.example.gabor.lynxproject.async;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.gabor.lynxproject.activity.SplashScreenActivity;
import com.example.gabor.lynxproject.interfaces.GeneralCallback;
import com.example.gabor.lynxproject.util.Constants;
import com.example.gabor.lynxproject.util.LogService;
import com.example.gabor.lynxproject.util.Util;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.UserRecoverableAuthException;

/**
 * Created by Gabor on 19.09.2015.
 */
public class RetrieveTokenTask extends AsyncTask<String, Void, Object> {

    private static final String TAG = RetrieveTokenTask.class.getSimpleName();

    private Context context;
    private String accountEmail;
    private GeneralCallback callback;

    public RetrieveTokenTask(Context con, GeneralCallback c) {
        context = con;
        callback = c;
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        accountEmail = Util.getUserEmail(context);
        LogService.log(TAG, "onPreExecute : " + accountEmail);
    }

    // make the default Constructor to private
    private RetrieveTokenTask() {
    }

    @Override
    protected Object doInBackground(String... params) {

        Object result = null;

        if (accountEmail == null) {
            return result;
        }


        try {

            result = GoogleAuthUtil.getAccountId(context, accountEmail);

        } catch (UserRecoverableAuthException e) {
            result = e;
        } catch (Exception e) {
            result = e;
        }

        return result;
    }

    @Override
    protected void onPostExecute(Object result) {
        super.onPostExecute(result);

        if (result instanceof UserRecoverableAuthException) {
            //we have to sign in
            Intent intent = ((UserRecoverableAuthException) result).getIntent();
            ((SplashScreenActivity) context).startActivityForResult(intent, ((SplashScreenActivity) context).REQ_SIGN_IN_REQUIRED);
        } else if (result instanceof Exception) {

            LogService.log(TAG, "Exception : " + ((Exception) result));
            Toast.makeText(context, "User not found", Toast.LENGTH_LONG);
        } else if (result instanceof String) {
            Constants.GOOGLE_UID = (String) result;
            LogService.log(TAG, "Constants.GOOGLE_UID : " + Constants.GOOGLE_UID);
        }

        if (callback != null) {
            callback.Callback(result);
        }

    }
}
