**Android Study Project**

Get Google UID,Get phone Contacts,
Realm database usage,
Pull to refresh,
Google map,
assynctask,
image resize,

**Set up git on calc (cmd):**

set PATH=C:\Program Files\Git\bin;\%PATH%

**Git tutorial:**

 https://www.youtube.com/watch?v=IBEbJUEDWZQ


**Force Git to overwrite local files**

git fetch --all
git reset --hard origin/master

OR If you are on some other branch

git reset --hard origin/your_branch

**Set up SSH for Git**

https://confluence.atlassian.com/bitbucket/set-up-ssh-for-git-728138079.html
OR
https://help.github.com/articles/generating-ssh-keys/